using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ThisBuchanan.com.Models;

namespace ThisBuchanan.com.Pages
{
    public class LearningModel : PageModel
    {

        public EquationModel equationModel { get; set; } = new EquationModel();

        [BindProperty]
        public string FirstName { get; set; }

        public void OnGet()
        {
          
        }

        public IActionResult OnPost()
        {
            return Page();
        }


    }
}
