using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ThisBuchanan.com.Models;

namespace ThisBuchanan.com.Pages.Forms
{
    public class AddPersonModel : PageModel
    {
        [BindProperty]
        public PersonModel Person { get; set; }
        public void OnGet()
        {
        }
        public IActionResult OnPost()
        {
            //if (ModelState.IsValid == false)
            //{
            //    return Page();
            //} else
            //{
            //    return RedirectToPage("/Index");
            //}
            return Page();
        }
    }
}
