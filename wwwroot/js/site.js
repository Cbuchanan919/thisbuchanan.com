﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.


// This shows any toasts on the current page with the showToast id
const toastButton = document.querySelector('#showToast');
if (toastButton !== undefined && toastButton !== null) {
    toastButton.addEventListener('click', event => {
        $(".toast").toast("show");
        const label = document.querySelector('#toastTimeAgo')
        let timeShown = new Date();
        label.textContent = timeShown.toLocaleTimeString("en-US");

    });
}



// This enables Carousels
//$('.carousel').carousel({
//    interval: 8000
//})

var myCarousel = document.querySelector('#cbCarousel')
var carousel = new bootstrap.Carousel(myCarousel, {
    interval: 5000,
    wrap: true
})