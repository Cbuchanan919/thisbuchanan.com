﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ThisBuchanan.com.Models
{

    public class EquationModel
    {


        public string GetSolutionExamples()
        {

            List<string> equations = new List<string>();
            equations.Add("1+1=?");
            equations.Add("123*45?=5?088");
            equations.Add("-5?*-1=5?");
            equations.Add("19--45=5?");
            equations.Add("??*??=302?");
            equations.Add("?*11=??");
            equations.Add("??*1=??");
            equations.Add("??*1=??");

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine("In this section, I had a coding wars challenge to design a function that would take a math expression");
            sb.AppendLine("(a string with question marks replacing 1 digit), " + "and have it figure out what the question marks were.");
            sb.AppendLine("(My next goal is to let you enter your own 'equation' and have it solve for '?')");
            sb.AppendLine("The following question marks are:");
            foreach (string equ in equations)
            {
                sb.AppendLine($"{equ} ({SolveExpression(equ)})");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Returns the correct numeral for question marks in equation strings. ('123*45?=5?088' returns 6), or ('-5?*-1=5?' returns 0) Returns -1 if no match found
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static int SolveExpression(string expression)
        {
            string[] pts = expression.Split("=");

            // p1 +-* p2 = ans
            string p1 = pts[0][0].ToString(); //adds the first digit to the p1
            string op = ""; //operator
            string p2 = ""; //second part
            string ans = pts[1];

            //discovering operator. end with subtraction last, due to negative numbers. 
            if (pts[0].Contains("+")) { op = "+"; }
            else if (pts[0].Contains("*")) { op = "*"; }
            else
            {
                op = "-"; //ignoring division
                bool p2Reached = false;
                for (int i = 1; i < pts[0].Length; i++)
                { //assign p1 & p2
                    string c = pts[0][i].ToString();
                    if (c == "-" && !p2Reached) { p2Reached = true; }
                    else if (!p2Reached) { p1 += c; }
                    else { p2 += c; }
                }
            }

            if (op == "+" || op == "*")
            { // assign p1 & p2
                string[] tmp = pts[0].Split(op);
                p1 = tmp[0];
                p2 = tmp[1];
            }

            List<string> unavailable = new List<string>();
            foreach (char c in expression) { unavailable.Add(c.ToString()); }

            for (int i = 0; i < 10; i++)
            {
                if (!unavailable.Contains(i.ToString()))
                {
                    int tryP1 = Int32.Parse(p1.Replace("?", i.ToString()));

                    int tryP2 = Int32.Parse(p2.Replace("?", i.ToString()));
                    int tryAns = Int32.Parse(ans.Replace("?", i.ToString()));
                    if (tryP1.ToString().Length == p1.Length && tryP2.ToString().Length == p2.Length && tryAns.ToString().Length == ans.Length)
                    {
                        int testAnswer = 0;
                        if (op == "+") { testAnswer = tryP1 + tryP2; }
                        else if (op == "*") { testAnswer = tryP1 * tryP2; }
                        else testAnswer = tryP1 - tryP2;
                        if (testAnswer == tryAns) { return i; }
                    }

                }
            }
            return -1; //nothing found

        }
    }
}
