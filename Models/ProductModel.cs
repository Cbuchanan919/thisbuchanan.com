using System;

namespace ThisBuchanan.com.Models
{
    public class ProductModel
    {
        public int ItemID { get; set; }
        public string? ItemTitle { get; set; }
        public string? ItemSummary { get; set; }
        

    }
}