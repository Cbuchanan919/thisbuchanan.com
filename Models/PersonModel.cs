﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ThisBuchanan.com.Models
{
    /// <summary>
    /// Simple model that stores basic info about a person.
    /// </summary>
    public class PersonModel
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public int YearBorn { get; set; }
        public bool IsAlive { get; set; }
        public int YearsOld()
        {
            return DateTime.Today.Year - YearBorn;
        }
        
    }
}
